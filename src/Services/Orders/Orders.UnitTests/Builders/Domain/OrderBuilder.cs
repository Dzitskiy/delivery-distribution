﻿using System;
using Orders.Core.Domain;
using Orders.Core.Other;

namespace Orders.UnitTests.Builders.Domain
{
    public static class OrderBuilder
    {
        public static Order CreateBase()
        {
            return new Order()
            {
                Id = Guid.Parse("f5ba283f-0daf-4e0d-a6df-057c828922df"),
                Code = "A00000",
                DepartureAddress = "Санкт-Петербург, Невский пр., д.1",
                DestinationAddress = "Москва, Красная пл., д.1",
                Weight = 10,
                Price = 1000,
                CreatedOn = new DateTime(2021, 01, 01),
                UpdatedOn = new DateTime(2021, 01, 01),
                OrderStateId = 1
            };
        }

        public static Order WithEmptyId(this Order order)
        {
            order.Id = Guid.Empty;
            return order;
        }

        public static Order WithDepartureAddress(this Order order, string adress)
        {
            order.DepartureAddress = adress;
            return order;
        }

        public static OrderSettings CreateSettings()
        {
            return new OrderSettings()
            {
                OrderPrefix = "A",
                OrderInitialNumber = "1",
                OrderNumberLenght = "5"
            };

        }
    }
}
