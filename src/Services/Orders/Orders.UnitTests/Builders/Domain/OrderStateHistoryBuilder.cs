﻿using System;
using Orders.Core.Domain;

namespace Orders.UnitTests.Builders.Domain
{
    public static class OrderStateHistoryBuilder
    {
        public static OrderStateHistory CreateBase()
        {
            return new OrderStateHistory()
            {
                Id = Guid.Parse("99663394-9664-4d40-b95b-f5083744ac1e"),
                UpdatedOn = new DateTime(2021, 03, 14)
            };
        }

        public static OrderStateHistory WithId(this OrderStateHistory stateHistory, Guid id)
        {
            stateHistory.Id = id;
            return stateHistory;
        }

        public static OrderStateHistory WithOrderId(this OrderStateHistory stateHistory, Guid orderId)
        {
            stateHistory.OrderId = orderId;
            return stateHistory;
        }

        public static OrderStateHistory WithStateId(this OrderStateHistory stateHistory, int stateId)
        {
            stateHistory.OrderStateId = stateId;
            return stateHistory;
        }

        public static OrderStateHistory WithUpdatedOn(this OrderStateHistory stateHistory, DateTime updatedOn)
        {
            stateHistory.UpdatedOn = updatedOn;
            return stateHistory;
        }
    }
}
