﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using Orders.Core.Abstraction.Repositories;
using Orders.Core.Application.Exceptions;
using Orders.Core.Application.Services;
using Orders.Core.Domain;
using Orders.UnitTests.Attributes;
using Orders.UnitTests.Builders.Domain;
using Xunit;

namespace Orders.UnitTests.Core.Application.Services.OrderServiceTests
{
    public class GetOrderByIdAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task GetOrderByIdAsync_OrderIsNotFound_ThrowsEntityNotFoundException(
            [Frozen] Mock<IRepository<Order>> mockRepository,
            [Frozen] OrderService orderService)
        {
            //arrange
            var orderId = OrderBuilder.CreateBase().Id;
            mockRepository.Setup(repo => repo.GetByIdAsync(orderId))
                .ReturnsAsync(default(Order));

            //act
            Func<Task<Order>> act = async () =>
                await orderService.GetOrderByIdAsync(orderId);

            //assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }

        [Theory, AutoMoqData]
        public async Task GetOrderByIdAsync_OrderExists_ReturnResponse(
            [Frozen] Mock<IRepository<Order>> mockRepository,
            [Frozen] OrderService orderService)
        {
            //arrange
            var order = OrderBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(order.Id))
                .ReturnsAsync(order);

            //act
            var result = await orderService.GetOrderByIdAsync(order.Id);

            //assert
            result.Should().BeEquivalentTo(order);
        }
    }
}