﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using Orders.Core.Abstraction.Repositories;
using Orders.Core.Application.Exceptions;
using Orders.Core.Application.Services;
using Orders.Core.Domain;
using Orders.UnitTests.Attributes;
using Orders.UnitTests.Builders.Domain;
using Xunit;

namespace Orders.UnitTests.Core.Application.Services.OrderServiceTests
{
    public class DeleteOrderAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task DeleteOrder_OrderNotFound_ThrowsEntityNotFoundException(
            [Frozen] Mock<IRepository<Order>> mockRepository,
            [Frozen] OrderService orderService)
        {
            //arrange
            var orderId = OrderBuilder.CreateBase().Id;
            mockRepository.Setup(repo => repo.GetByIdAsync(orderId))
                .ReturnsAsync(default(Order));

            //act
            Func<Task> act = async () =>
                await orderService.DeleteOrderAsync(orderId);

            //assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }

        [Theory, AutoMoqData]
        public async Task DeleteOrder_OrderExists_DeleteInvoked(
            [Frozen] Mock<IRepository<Order>> mockRepository,
            [Frozen] OrderService orderService)
        {
            //arrange
            var order = OrderBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(order.Id))
                .ReturnsAsync(order);

            //act
            await orderService.DeleteOrderAsync(order.Id);

            //assert
            mockRepository.Verify(repo =>
                repo.DeleteAsync(order), Times.Once);
        }
    }
}