﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using Orders.Core.Abstraction;
using Orders.Core.Abstraction.Repositories;
using Orders.Core.Application.Exceptions;
using Orders.Core.Application.Services;
using Orders.Core.Domain;
using Orders.UnitTests.Attributes;
using Orders.UnitTests.Builders.Domain;
using Xunit;

namespace Orders.UnitTests.Core.Application.Services.OrderServiceTests
{
    public class EditOrderAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task EditOrderAsync_OrderNotFound_ThrowsEntityNotFoundException(
           [Frozen] Mock<IRepository<Order>> mockRepository,
           [Frozen] OrderService orderService)
        {
            //arrange
            var orderId = OrderBuilder.CreateBase().Id;
            mockRepository.Setup(repo => repo.GetByIdAsync(orderId))
                .ReturnsAsync(default(Order));

            //act
            Func<Task> act = async () =>
                await orderService.EditOrderAsync(orderId, It.IsAny<Order>());
                        
            //assert
            await act.Should().ThrowAsync<EntityNotFoundException>();

        }

        [Theory, AutoMoqData]
        public async Task EditOrderAsync_OrderExists_UpdatedOnIsNow(
            [Frozen] Mock<IRepository<Order>> mockRepository,
            [Frozen] Mock<ICurrentDateTimeProvider> mockDateTimeProvider,
            [Frozen] OrderService orderService)
        {
            //arrange
            var order = OrderBuilder.CreateBase();
            var updatedOrder = OrderBuilder
                .CreateBase()
                .WithEmptyId()
                .WithDepartureAddress("newAddress");

            mockDateTimeProvider.Setup(provider => provider.CurrentDateTime)
                .Returns(new DateTime(2021, 02, 15));
            mockRepository.Setup(repo => repo.GetByIdAsync(order.Id))
                .ReturnsAsync(order);

            //act
            await orderService.EditOrderAsync(order.Id, updatedOrder);

            //assert
            order.UpdatedOn.Should().Be(mockDateTimeProvider.Object.CurrentDateTime);            
        }

        [Theory, AutoMoqData]
        public async Task EditOrderAsync_OrderExists_UpdateInvoked(
            [Frozen] Mock<IRepository<Order>> mockRepository,
            [Frozen] OrderService orderService)
        {
            //arrange
            var order = OrderBuilder.CreateBase();
            var updatedOrder = OrderBuilder.CreateBase()
                .WithEmptyId()
                .WithDepartureAddress("gee");
            
            mockRepository.Setup(repo => repo.GetByIdAsync(order.Id))
                .ReturnsAsync(order);

            //act
            await orderService.EditOrderAsync(order.Id, updatedOrder);

            //assert
            mockRepository.Verify(repo =>
                repo.UpdateAsync(order), Times.Once);
        }
    }
}
