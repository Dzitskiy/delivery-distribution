﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Moq;
using Orders.Core.Abstraction;
using Orders.Core.Application.Services;
using Orders.UnitTests.Attributes;
using Orders.UnitTests.Builders.Domain;
using Xunit;

namespace Orders.UnitTests.Core.Application.Services.OrderStateHistoryServiceTests
{
    public class AddOrderStateAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task AddOrderStateAsync_CorrectInput_UpdatedOnIsNow(
            [Frozen] Mock<ICurrentDateTimeProvider> mockDateTime,
            [Frozen] OrderStateHistoryService sut)
        {
            //arrange
            var stateHistory = OrderStateHistoryBuilder.CreateBase()
                .WithOrderId(OrderBuilder.CreateBase().Id)
                .WithStateId(1);
            var now = new DateTime(2021, 03, 15);
            
            mockDateTime.Setup(mock => mock.CurrentDateTime)
                .Returns(now);
            
            //act
            await sut.AddOrderStateAsync(stateHistory);
            
            //assert
            stateHistory.UpdatedOn.Should().Be(now);
        }
    }
}