using System;
using System.Reflection;
using AutoMapper;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NSwag.AspNetCore;
using Orders.Core.Abstraction;
using Orders.Core.Abstraction.Repositories;
using Orders.Core.Abstraction.Services;
using Orders.Core.Abstraction.Strategies;
using Orders.Core.Application;
using Orders.Core.Application.Services;
using Orders.Core.Application.Strategies;
using Orders.Core.Domain;
using Orders.Core.Other;
using Orders.DataAccess.Data;
using Orders.DataAccess.Data.DbInitializer;
using Orders.DataAccess.Repositories;
using Orders.WebHost.Helpers;
using Orders.WebHost.Options;
using Serilog;

namespace Orders.WebHost
{
    public class Startup
    {
        
        public IConfiguration Configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration; 
        }
        
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder
                        .SetIsOriginAllowed((host) => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                });
            });
            
            services.AddControllers()
                .AddMvcOptions(options => options.SuppressAsyncSuffixInActionNames = false)
                .AddFluentValidation(opt =>
                {
                    opt.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                    opt.LocalizationEnabled = false;
                });
            
            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());
            
            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddScoped<IRepository<Order>, OrdersRepository>();
            services.AddScoped<IRepository<OrderStateHistory>, OrderStateHistoryRepository>();
            
            services.AddScoped<ICurrentDateTimeProvider, CurrentDateTimeProvider>();

            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOrderStateHistoryService, OrderStateHistoryService>();
            
            services.AddScoped<IUpdateOrderStateStrategy, UpdateOrderStateStrategy>();
            services.AddScoped<IComputeOrderPriceStrategy, RandomOrderPriceStrategy>();
            
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            
            var operationSettingsSection = Configuration.GetSection("OperationSettings");
            services.Configure<OperationSettings>(operationSettingsSection);
            
            services.AddDbContext<DataContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("PostgresConnection"));
            });

            services.AddCustomAuthentication(Configuration);
            services.AddCustomAuthorization();

            services.AddSwaggerWithAuth(Configuration);
            
            services.AddMassTransitRabbitMq(Configuration);
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            app.UseExceptionHandler(err => err.UseCustomErrors(env));
            
            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(options =>
            {
                options.DocExpansion = "list";
                options.OAuth2Client = new OAuth2ClientSettings
                {
                    ClientId = "ordersswaggerui",
                    AppName = "Orders Swagger UI"
                };
            });
            
            //app.UseHttpsRedirection();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints
                    .MapControllers()
                    .RequireAuthorization(Policies.OrdersAccess);
            });
            
            dbInitializer.InitializeDb();
        }
    }
}