﻿using System;

namespace Orders.WebHost.Dto.Order
{
    public record OrderShortResponse
    {
        public Guid Id { get; set; }
        
        public DateTime CreatedOn { get; set; }
        
        public StateResponse OrderState { get; set; }
        
        public double Price { get; set; }
        
        public bool IsPaid { get; set; }
        
        public string? Code { get; set; }
    }
}