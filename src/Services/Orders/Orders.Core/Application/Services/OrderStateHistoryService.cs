﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Orders.Core.Abstraction;
using Orders.Core.Abstraction.Repositories;
using Orders.Core.Abstraction.Services;
using Orders.Core.Domain;

namespace Orders.Core.Application.Services
{
    public class OrderStateHistoryService
        :IOrderStateHistoryService
    {
        private readonly IRepository<OrderStateHistory> _orderStateHistoryRepository;
        private readonly ICurrentDateTimeProvider _currentDateTimeProvider;

        public OrderStateHistoryService(
            IRepository<OrderStateHistory> orderStateHistoryRepository,
            ICurrentDateTimeProvider currentDateTimeProvider)
        {
            _orderStateHistoryRepository = orderStateHistoryRepository;
            _currentDateTimeProvider = currentDateTimeProvider;
        }
        
        public async Task AddOrderStateAsync(OrderStateHistory newOrderStateHistory)
        {
            newOrderStateHistory.UpdatedOn = _currentDateTimeProvider.CurrentDateTime;
            await _orderStateHistoryRepository.AddAsync(newOrderStateHistory);
        }

        public async Task<IEnumerable<OrderStateHistory>> GetHistoryForOrderByIdAsync(Guid orderId)
        {
            var historyList = await _orderStateHistoryRepository
                .GetOnConditionAsync(item => item.OrderId == orderId);
            return historyList;
        }
    }
}