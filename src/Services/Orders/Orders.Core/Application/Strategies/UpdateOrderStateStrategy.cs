﻿using System;
using System.Threading.Tasks;
using Orders.Core.Abstraction.Services;
using Orders.Core.Abstraction.Strategies;

namespace Orders.Core.Application.Strategies
{
    public class UpdateOrderStateStrategy: IUpdateOrderStateStrategy
    {

        private readonly IOrderService _orderService;

        public UpdateOrderStateStrategy(IOrderService orderService)
        {
            _orderService = orderService;
        }

        public async Task UpdateOrderStateAsync(Guid orderId, int deliveryStateId)
        {
            var newOrderState = 0;
            switch (deliveryStateId)
            {
                case 1:
                    newOrderState = 2;
                    break;
                case 2:
                    newOrderState = 2;
                    break;
                case 3:
                    newOrderState = 3;
                    break;
                case 4:
                    newOrderState = 4;
                    break;
                case 5:
                    newOrderState = 6;
                    break;
            }

            if (newOrderState != 0)
            {
                await _orderService.EditOrderStatusAsync(orderId, newOrderState);
            }
        }
    }
}