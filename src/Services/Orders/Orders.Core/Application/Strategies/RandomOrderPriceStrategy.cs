﻿using System;
using System.Threading.Tasks;
using Orders.Core.Abstraction.Strategies;

namespace Orders.Core.Application.Strategies
{
    public class RandomOrderPriceStrategy: IComputeOrderPriceStrategy
    {
        public async Task<double> ComputePriceAsync(double weight, string departureAddress, string destinationAddress)
        {
            return await Task.FromResult(new Random().Next(10, 1000));
        }
    }
}