using System;

namespace Orders.Core.Abstraction
{
    public interface ICurrentDateTimeProvider
    {
        DateTime CurrentDateTime { get; }
    }
}