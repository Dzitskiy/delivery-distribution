﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Orders.Core.Domain;

namespace Orders.Core.Abstraction.Services
{
    /// <summary>
    /// Сервис для работы с Заказами
    /// </summary>
    public interface IOrderService
    {
        /// <summary>
        /// Получить список всех заказов
        /// </summary>
        /// <param name="customerId">id покупателя</param>
        Task<IEnumerable<Order>> GetOrdersAsync(Guid? customerId = null);

        /// <summary>
        /// Получить данные заказа по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Order> GetOrderByIdAsync(Guid id);

        /// <summary>
        /// Создать новый заказ
        /// </summary>
        /// <param name="newOrder"></param>
        /// <returns></returns>
        Task<Guid> AddOrderAsync(Order newOrder);

        /// <summary>
        /// Изменить данные заказа по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatedOrder"></param>
        /// <returns></returns>
        Task EditOrderAsync(Guid id, Order updatedOrder);

        /// <summary>
        /// Удалить заказ по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteOrderAsync(Guid id);

        /// <summary>
        /// Изменить статус заказа по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newStateId"></param>
        /// <returns></returns>
        Task EditOrderStatusAsync(Guid id, int newStateId);

        /// <summary>
        /// Вычисление стоимости заказа
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="departureAddress"></param>
        /// <param name="destinationAddress"></param>
        /// <returns></returns>
        Task<double> ComputePrice(double weight, string departureAddress, string destinationAddress);

        /// <summary>
        /// Отметить заказ оплаченным по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task SetPaidByIdAsync(Guid id);
    }
}