﻿using System.Threading.Tasks;

namespace Orders.Core.Abstraction.Strategies
{
    public interface IComputeOrderPriceStrategy
    {
        Task<double> ComputePriceAsync(double weight, string departureAddress, string destinationAddress);
    }
}