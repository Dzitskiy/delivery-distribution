using System;

namespace Orders.Core.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}