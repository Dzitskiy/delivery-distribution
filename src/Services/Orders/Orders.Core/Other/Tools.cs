﻿using System;

namespace Orders.Core.Other
{
    public class Tools
    {
        public static string GetNextOrderCode(string maxOrderCode, OrderSettings orderSettings)
        {
            int maxOrderNo;
            Int32.TryParse(maxOrderCode.Substring(orderSettings.OrderPrefix.Length + 1), out maxOrderNo);
            if (maxOrderNo > 0)
                return orderSettings.OrderPrefix + (maxOrderNo + 1).ToString("D" + orderSettings.OrderNumberLenght);
            else
                return GetInitialOrderCode(orderSettings);
        }
        public static string GetInitialOrderCode(OrderSettings orderSettings)
        {
            int orderNo;
            Int32.TryParse(orderSettings.OrderInitialNumber, out orderNo);
            if (orderNo == 0) orderNo = 1;
            return orderSettings.OrderPrefix + orderNo.ToString("D" + orderSettings.OrderNumberLenght);
        }
    }
}
