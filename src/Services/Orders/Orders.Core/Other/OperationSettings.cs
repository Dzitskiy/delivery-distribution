﻿namespace Orders.Core.Other
{
    public class OperationSettings
    {
        public OrderSettings OrderSettings { get; set; }                
    }

    public class OrderSettings
    {
        public string OrderPrefix { get; set; }
        public string OrderNumberLenght { get; set; }
        public string OrderInitialNumber { get; set; }
        public string OrderMaxNumber { get; set; }

    }
}
