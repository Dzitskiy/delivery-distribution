using Microsoft.EntityFrameworkCore;
using Orders.Core.Domain;
using Orders.Core.Enumerations;
using Orders.DataAccess.EntityConfigurations;

namespace Orders.DataAccess.Data
{
    public class DataContext: DbContext
    {
        
        public DbSet<Order> Orders { get; set; }
        
        public DbSet<OrderState> OrderStates { get; set; }
        
        public DbSet<OrderStateHistory> OrderStateHistories { get; set; }
        
        public DataContext()
        {
            
        }
        
        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new OrderStateEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new OrderEntityTypeConfiguration());
            
            base.OnModelCreating(modelBuilder);
        }
    }
}