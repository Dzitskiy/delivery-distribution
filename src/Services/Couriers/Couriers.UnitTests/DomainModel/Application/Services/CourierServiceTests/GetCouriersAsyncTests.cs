﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Couriers.Core.Abstraction.Repositories;
using Couriers.Core.Domain;
using Couriers.Core.Services;
using Couriers.UnitTests.Attributes;
using Couriers.UnitTests.Builders.DomainModel.Domain;
using Moq;
using Xunit;

namespace Couriers.UnitTests.DomainModel.Application.Services.CourierServiceTests
{
    public class GetCouriersAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task GetCouriersAsync_CourierNotFound_ReturnsEmptyCollection(
            [Frozen] Mock<IRepository<Courier>> mockRepository,
            [Frozen] CourierService courierService
            )
        {
            //Arrange
            mockRepository.Setup(repo => repo.GetAllAsync()).ReturnsAsync(new List<Courier>());
                
            //Act
            var result =  await courierService.GetCouriersAsync();

            //Assert
            result.Should().BeEmpty();
        }

        [Theory, AutoMoqDataWithMapper]
        public async Task GetCouriersAsync_CourierFound_ShouldReturnFoundCourierList(
            [Frozen] Mock<IRepository<Courier>> mockRepository,
            [Frozen] CourierService courierService
            )
        {
            //Arrange
            var courier = CourierBuilder.CreateBase();
            var courierList = new List<Courier>() {courier};
            mockRepository.Setup(repo => repo.GetAllAsync()).ReturnsAsync(courierList);

            //Act
            var result = await courierService.GetCouriersAsync();

            //Assert
            result.Should().BeEquivalentTo(courierList, opt => opt.ExcludingMissingMembers());
        }
    }
}