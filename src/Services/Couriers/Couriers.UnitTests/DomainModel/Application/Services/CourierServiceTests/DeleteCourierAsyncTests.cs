﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FluentAssertions;
using Couriers.Core.Abstraction.Repositories;
using Couriers.Core.Domain;
using Couriers.Core.Services;
using Couriers.UnitTests.Attributes;
using Couriers.UnitTests.Builders.DomainModel.Domain;
using Moq;
using Xunit;
using Couriers.Core.Application.Exceptions;

namespace Couriers.UnitTests.DomainModel.Application.Services.CourierServiceTests
{
    public class DeleteCourierAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task DeleteCourierAsync_CourierNotFound_ShouldThrowEntityNotFoundException(
            [Frozen] Mock<IRepository<Courier>> mockRepository,
            [Frozen] CourierService courierService
        )
        {
            //Arrange
            var courier = CourierBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(courier.Id)).ReturnsAsync(default(Courier));
                
            //Act
            Func<Task> act = async () => await courierService.DeleteCourierAsync(courier.Id);

            //Assert
            await act.Should().ThrowAsync<EntityNotFoundException>();
        }
        
        [Theory, AutoMoqData]
        public async Task DeleteCourierAsync_SuccessfullyExecuted_ShouldInvokeDeleteMethod(
            [Frozen] Mock<IRepository<Courier>> mockRepository,
            [Frozen] CourierService courierService
        )
        {
            //Arrange
            var courier = CourierBuilder.CreateBase();
            mockRepository.Setup(repo => repo.GetByIdAsync(courier.Id)).ReturnsAsync(courier);
                
            //Act
            await courierService.DeleteCourierAsync(courier.Id);

            //Assert
            mockRepository.Verify(repo => repo.DeleteAsync(courier), Times.Once);
        }
    }
}