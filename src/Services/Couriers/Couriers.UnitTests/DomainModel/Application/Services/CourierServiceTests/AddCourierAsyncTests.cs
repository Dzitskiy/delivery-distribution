﻿using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Couriers.Core.Abstraction.Repositories;
using Couriers.Core.Domain;
using Couriers.Core.Services;
using Couriers.UnitTests.Attributes;
using Couriers.UnitTests.Builders.DomainModel.Domain;
using Moq;
using Xunit;

namespace Couriers.UnitTests.DomainModel.Application.Services.CourierServiceTests
{
    public class AddCourierAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task AddCourierAsync_SuccessfullyExecuted_ShouldInvokeAddMethod(
            [Frozen] Mock<IRepository<Courier>> mockRepository,
            [Frozen] Mock<IRepository<Vehicle>> mockVehicleRepository,
            [Frozen] CourierService courierService
        )
        {
            //Arrange
            var courier = CourierBuilder.CreateBase();
            var vehicle = VehicleBuilder.CreateBase();
            mockRepository.Setup(repo => repo.AddAsync(courier)).ReturnsAsync(courier.Id);
            mockVehicleRepository.Setup(repo => repo.GetByIdAsync(courier.VehicleId)).ReturnsAsync(vehicle);

            //Act
            await courierService.AddCourierAsync(courier);

            //Assert
            mockRepository.Verify(repo => repo.AddAsync(courier), Times.Once);
        }
    }
}