using System;
using System.Reflection;
using AutoMapper;
using Couriers.Core.Application;
using Couriers.Core.Abstraction;
using Couriers.Core.Abstraction.Repositories;
using Couriers.Core.Abstraction.Services;
using Couriers.Core.Domain;
using Couriers.Core.Services;
using Couriers.DataAccess.Data;
using Couriers.DataAccess.Data.DbInitializer;
using Couriers.DataAccess.Repositories;
using Couriers.WebHost.Helpers;
using Couriers.WebHost.Options;
using FluentValidation.AspNetCore;
using MassTransit;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using DeliveryDistribution.Integration.Contracts;
using NSwag.AspNetCore;


namespace Couriers.WebHost
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder
                        .SetIsOriginAllowed((host) => true)
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                });
            });
            
            services.AddControllers()
                .AddMvcOptions(options => options.SuppressAsyncSuffixInActionNames = false)
                .AddFluentValidation(opt =>
                {
                    opt.RegisterValidatorsFromAssembly(Assembly.GetExecutingAssembly());
                    opt.LocalizationEnabled = false;
                });

            services.AddMediatR(AppDomain.CurrentDomain.GetAssemblies());

            services.AddScoped<IDbInitializer, EfDbInitializer>();
            services.AddScoped<IRepository<Vehicle>, VehiclesRepository>();
            services.AddScoped<IRepository<Courier>, CouriersRepository>();            
            services.AddScoped<ICurrentDateTimeProvider, CurrentDateTimeProvider>();
            services.AddScoped<ICourierService, CourierService>();
            
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services.AddDbContext<DataContext>(options =>
            {
                options.UseNpgsql(Configuration.GetConnectionString("PostgresConnection"));
            });

            services.AddCustomAuthentication(Configuration);
            services.AddCustomAuthorization();

            services.AddSwaggerWithAuth(Configuration);

            services.AddMassTransitRabbitMq(Configuration);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            app.UseExceptionHandler(err => err.UseCustomErrors(env));

            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(options =>
            {
                options.DocExpansion = "list";
                options.OAuth2Client = new OAuth2ClientSettings
                {
                    ClientId = "couriersswaggerui",
                    AppName = "Couriers Swagger UI"
                };
            });

         //   app.UseHttpsRedirection();

            app.UseSerilogRequestLogging();

            app.UseRouting();

            app.UseCors();

            app.UseAuthentication();
            app.UseAuthorization();
            
            app.UseEndpoints(endpoints =>
            {
                endpoints
                    .MapControllers()
                    .RequireAuthorization(Policies.CouriersAccess);
            });

            dbInitializer.InitializeDb();
        }
    }
    
}