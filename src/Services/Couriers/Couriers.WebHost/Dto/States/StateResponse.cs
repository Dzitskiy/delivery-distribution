﻿namespace Couriers.WebHost.Dto.States
{
    public record StateResponse
    {
        public int Id { get; set; }
        public string State { get; set; }
    }
}