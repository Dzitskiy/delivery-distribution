﻿namespace Couriers.WebHost.Dto.Courier
{
    public class EditCourierStatusRequest
    {
        public int State { get; set; }
    }
}