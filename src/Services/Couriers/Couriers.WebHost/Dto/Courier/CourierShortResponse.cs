using System;

namespace Couriers.WebHost.Dto.Courier
{
    public record CourierShortResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string VehicleName { get; set; }
        public int VehicleLoadCapacity { get; set; }
        public string State { get; set; }
    }
}