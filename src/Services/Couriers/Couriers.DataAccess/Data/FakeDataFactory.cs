﻿using Couriers.Core.Domain;
using System;
using System.Collections.Generic;

namespace Couriers.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Courier> Couriers => new List<Courier>()
        {
            new Courier()
            {
                Id = Guid.Parse("fefed0c7-53f0-4752-933a-331a389a3277"),
                Name = "Иван Сергеев",
                PhoneNumber = "123456789",
                VehicleId = Guid.Parse("312348d4-392a-4ed7-905d-13243f294900")
            },
            new Courier()
            {
                Id = Guid.Parse("38ece04d-8d9b-4b0d-91b2-538cd05d8300"),
                Name = "Петр Андреев",
                PhoneNumber = "987654321",
                VehicleId = Guid.Parse("c35734ae-48b4-47a4-bda6-55c0c872dd39")
            },
        };

        public static IEnumerable<Vehicle> Vehicles => new List<Vehicle>()
        {
            new Vehicle()
            {
                Id = Guid.Parse("3101f6a2-44be-4f97-91d5-4a4c2a921c9f"),
                Name = "Без транспортного средства",
                LoadCapacity = 20
            },
            new Vehicle()
            {
                Id = Guid.Parse("312348d4-392a-4ed7-905d-13243f294900"),
                Name = "Велосипед",
                LoadCapacity = 40
            },
            new Vehicle()
            {
                Id = Guid.Parse("c35734ae-48b4-47a4-bda6-55c0c872dd39"),
                Name = "Легковой автомобиль",
                LoadCapacity = 200
            },
            new Vehicle()
            {
                Id = Guid.Parse("28902d54-33a5-4de1-9f8e-203a16b0ade4"),
                Name = "Грузовой автомобиль",
                LoadCapacity = 3000
            },
        };
    }
}
