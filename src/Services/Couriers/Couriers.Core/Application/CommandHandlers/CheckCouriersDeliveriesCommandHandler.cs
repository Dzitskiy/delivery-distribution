using System;
using System.Threading;
using System.Threading.Tasks;
using Couriers.Core.Application.Commands;
using DeliveryDistribution.Integration.Contracts;
using MassTransit;
using MediatR;

namespace Couriers.Core.Application.CommandHandlers
{
    public class CheckCouriersDeliveriesCommandHandler
        :IRequestHandler<CheckCouriersDeliveriesCommand, bool>
    {
        private readonly IRequestClient<CheckCouriersDeliveries> _requestClient;

        public CheckCouriersDeliveriesCommandHandler(
            IRequestClient<CheckCouriersDeliveries> requestClient)
        {
            _requestClient = requestClient;
        }

        public async Task<bool> Handle(CheckCouriersDeliveriesCommand request, CancellationToken cancellationToken)
        {
            var response =  await _requestClient.GetResponse<CheckCouriersDeliveriesResult>(new
            {
                CourierId = request.CourierId
            }, cancellationToken);
            return response.Message.Result;
        }
    }
}