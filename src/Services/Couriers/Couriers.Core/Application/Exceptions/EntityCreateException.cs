﻿using System;

namespace Couriers.Core.Application.Exceptions
{
    public class EntityCreateException : Exception
    {
        public EntityCreateException()
        {
        }

        public EntityCreateException(string? message) : base(message)
        {
        }

        public EntityCreateException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}