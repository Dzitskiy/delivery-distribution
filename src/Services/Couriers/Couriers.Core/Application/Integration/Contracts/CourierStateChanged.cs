﻿using System;

namespace DeliveryDistribution.Integration.Contracts
{
    public interface CourierStateChanged
    {
        public Guid CourierId { get;}
        public int CourierStateId { get;}
    }
}