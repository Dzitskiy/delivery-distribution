﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Couriers.Core.Abstraction.Repositories;
using Couriers.Core.Abstraction.Services;
using Couriers.Core.Application.Commands;
using Couriers.Core.Application.Events;
using Couriers.Core.Application.Exceptions;
using Couriers.Core.Domain;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Couriers.Core.Services
{
    public class CourierService : ICourierService
    {

        private readonly IRepository<Courier> _courierRepository;
        private readonly IRepository<Vehicle> _vehicleRepository;
        private readonly ILogger<CourierService> _logger;
        private readonly IMediator _mediator;

        public CourierService(
            IRepository<Courier> courierRepository,
            IRepository<Vehicle> vehicleRepository,
            ILogger<CourierService> logger,
            IMediator mediator)
        {
            _courierRepository = courierRepository;
            _vehicleRepository = vehicleRepository;
            _logger = logger;
            _mediator = mediator;
        }

        public async Task<IEnumerable<Courier>> GetCouriersAsync()
        {
            var couriers = await _courierRepository.GetAllAsync();
            
            return couriers;
        }

        public async Task<Courier> GetCourierByIdAsync(Guid id)
        {
            var courier = await _courierRepository.GetByIdAsync(id);
            if (courier == null)
            {
                _logger.LogWarning("Courier with id: {@CourierId} not found", id);
                throw new EntityNotFoundException(id);
            }
            return courier;
        }

        public async Task<Guid> AddCourierAsync(Courier newCourier)
        {
            var vehicle = await _vehicleRepository.GetByIdAsync(newCourier.VehicleId);
            if (vehicle == null)
            {
                _logger.LogWarning("Courier with id: {@CourierId} not found", newCourier.VehicleId);
                throw new EntityNotFoundException(newCourier.VehicleId);
            }
            await _courierRepository.AddAsync(newCourier);
            return newCourier.Id;
        }

        public async Task EditCourierAsync(Guid id, Courier updatedCourier)
        {
            var courier = await _courierRepository.GetByIdAsync(id);
            if (courier == null)
            {
                _logger.LogWarning("Courier with id: {@CourierId} not found", id);
                throw new EntityNotFoundException(id);
            }
            var vehicle = await _vehicleRepository.GetByIdAsync(updatedCourier.VehicleId);
            if (vehicle == null)
            {
                _logger.LogWarning("Vehicle with id: {@VehicleId} not found", updatedCourier.VehicleId);
                throw new EntityNotFoundException(updatedCourier.VehicleId);
            }

            courier.Name = updatedCourier.Name;
            courier.PhoneNumber = updatedCourier.PhoneNumber;
            courier.VehicleId = updatedCourier.VehicleId;

            await _courierRepository.UpdateAsync(courier);
        }

        public async Task DeleteCourierAsync(Guid id)
        {
            var courier = await _courierRepository.GetByIdAsync(id);
            if (courier == null)
            {
                _logger.LogWarning("Courier with id: {@CourierId} not found", id);
                throw new EntityNotFoundException(id);
            }

            var courierHaveDeliveries = await _mediator.Send(new CheckCouriersDeliveriesCommand()
            {
                CourierId = courier.Id
            });
            
            if (courierHaveDeliveries)
            {
               _logger.LogWarning("Courier with id: {@CourierId} can not be removed due to existing deliveries", id);
                throw new EntityDeleteException($"Courier with id: {id} can not be removed due to existing deliveries");
            }

            await _courierRepository.DeleteAsync(courier);
        }
        
        public async Task EditCourierStatusAsync(Guid id, int newStateId)
        {
            var courier = await _courierRepository.GetByIdAsync(id);
            if (courier == null)
            {
                _logger.LogWarning("courier with id: {@CourierId} not found", id);
                throw new EntityNotFoundException(id);
            }

            courier.CourierStateId = newStateId;
            await _courierRepository.UpdateAsync(courier);
            _logger.LogInformation("Updating courier status: {@Courier}", courier);

            await _mediator.Publish(new CourierStateUpdatedEvent()
            {
                CourierId = id,
                CourierStateId = newStateId
            });
        }

        public async Task UpdateCourierVehicleByIdAsync(Guid id, Guid vehicleId)
        {
            var courier = await _courierRepository.GetByIdAsync(id);
            if (courier == null)
            {
                _logger.LogWarning("Courier with id: {@CourierId} not found", id);
                throw new EntityNotFoundException(id);
            }
            var vehicle = await _vehicleRepository.GetByIdAsync(vehicleId);
            if (vehicle == null)
            {
                _logger.LogWarning("Vehicle with id: {@VehicleId} not found", vehicleId);
                throw new EntityNotFoundException(vehicleId);
            }

            courier.VehicleId = vehicleId;

            await _courierRepository.UpdateAsync(courier);
        }
    }

}
