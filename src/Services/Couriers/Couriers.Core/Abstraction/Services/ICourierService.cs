﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Couriers.Core.Domain;

namespace Couriers.Core.Abstraction.Services
{
    /// <summary>
    /// Сервис для работы с курьерами
    /// </summary>
    public interface ICourierService
    {
        /// <summary>
        /// Получить список курьеров
        /// </summary>
        Task<IEnumerable<Courier>> GetCouriersAsync();

        /// <summary>
        /// Получить данные курьера по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Courier> GetCourierByIdAsync(Guid id);

        /// <summary>
        /// Создать нового курьера
        /// </summary>
        /// <param name="newCourier"></param>
        /// <returns></returns>
        Task<Guid> AddCourierAsync(Courier newCourier);

        /// <summary>
        /// Изменить данные курьера по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updatedCourier"></param>
        /// <returns></returns>
        Task EditCourierAsync(Guid id, Courier updatedCourier);

        /// <summary>
        /// Удалить курьера по идентификатору вместе с выданными ему доставками 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteCourierAsync(Guid id);

        /// <summary>
        /// Изменить статус курьера
        /// </summary>
        /// <param name="id"></param>
        /// <param name="newStateId"></param>
        /// <returns></returns>
        Task EditCourierStatusAsync(Guid id, int newStateId);

        /// <summary>
        /// Изменить транспортное средство курьера
        /// </summary>
        /// <param name="id"></param>
        /// <param name="vehicleId"></param>
        /// <returns></returns>
        Task UpdateCourierVehicleByIdAsync(Guid id, Guid vehicleId);

    }
}
