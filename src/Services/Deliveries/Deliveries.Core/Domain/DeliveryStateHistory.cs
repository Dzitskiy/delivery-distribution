﻿using System;
using Deliveries.Core.Enumerations;

namespace Deliveries.Core.Domain
{
    public class DeliveryStateHistory:BaseEntity
    {
        /// <summary>
        /// Id доставки
        /// </summary>
        public Guid DeliveryId { get; set; }
        
        /// <summary>
        /// Доставка
        /// </summary>
        public Delivery? Delivery { get; set; }
        
        /// <summary>
        /// Id статуса доставки
        /// </summary>
        public int DeliveryStateId { get; set; }
        
        /// <summary>
        /// Статус доставки
        /// </summary>
        public DeliveryState? DeliveryState { get; set; }
        
        /// <summary>
        /// Время обновления состояния доставки
        /// </summary>
        public DateTime UpdatedOn { get; set; }
    }
}