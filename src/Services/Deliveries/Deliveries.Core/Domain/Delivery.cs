using System;
using Deliveries.Core.Enumerations;

namespace Deliveries.Core.Domain
{
    public class Delivery:
        BaseEntity
    {
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedOn { get; set; }
        
        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime UpdatedOn { get; set; }
        
        /// <summary>
        /// Id текущего состояния доставки
        /// </summary>
        public int DeliveryStateId { get; set; }
        
        /// <summary>
        /// Текущее состояние доставки
        /// </summary>
        public DeliveryState? DeliveryState { get; set; }

        /// <summary>
        /// Id курьера, выполняющего доставку
        /// </summary>
        public Guid? CourierId { get; set; } = null;
        
        /// <summary>
        /// Id заказа, для которого выполняется доставка
        /// </summary>
        public Guid OrderId { get; set; }
        
    }
}