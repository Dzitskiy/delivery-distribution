﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Deliveries.Core.Domain;

namespace Deliveries.Core.Abstraction.Services
{
    public interface IDeliveryService
    {
        /// <summary>
        /// Получение списка всех доставок
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Delivery>> GetDeliveriesAsync();
        
        /// <summary>
        /// Добавление новой доставки
        /// </summary>
        /// <param name="newDelivery"></param>
        /// <returns></returns>
        Task<Guid> AddDeliveryAsync(Delivery newDelivery);

        /// <summary>
        /// Получение доставки по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Delivery> GetDeliveryByIdAsync(Guid id);

        /// <summary>
        /// Получение списка доставок по id курьера 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<IEnumerable<Delivery>> GetCourierDeliveriesAsync(Guid? id);

        /// <summary>
        /// Получение списка доставок по id заказа
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<IEnumerable<Delivery>> GetOrderDeliveriesAsync(Guid id);

        /// <summary>
        /// Изменение доставки по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="editedDelivery"></param>
        /// <returns></returns>
        [Obsolete]
        Task EditDeliveryAsync(Guid id, Delivery editedDelivery);


        /// <summary>
        /// Изменение статуса доставки по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="stateId"></param>
        /// <returns></returns>
        Task UpdateDeliveryStateByIdAsync(Guid id, int stateId);

        
        /// <summary>
        /// Изменение курьера, которому назначена доставка по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="courierId"></param>
        /// <returns></returns>
        Task UpdateDeliveryCourierByIdAsync(Guid id, Guid courierId);

        /// <summary>
        /// Удаление доставки по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteDeliveryByIdAsync(Guid id);
    }
}
