﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Deliveries.Core.Domain;

namespace Deliveries.Core.Abstraction.Services
{
    public interface IDeliveryStateHistoryService
    {
        /// <summary>
        /// Добавление состояния в историю
        /// </summary>
        /// <param name="deliveryStateHistory"></param>
        /// <returns></returns>
        Task AddDeliveryStateAsync(DeliveryStateHistory deliveryStateHistory);

        /// <summary>
        /// Получение истории состояний для выбранной доставки
        /// </summary>
        /// <param name="deliveryId"></param>
        /// <returns></returns>
        Task<IEnumerable<DeliveryStateHistory>> GetHistoryForDeliveryByIdAsync(Guid deliveryId);
    }
}