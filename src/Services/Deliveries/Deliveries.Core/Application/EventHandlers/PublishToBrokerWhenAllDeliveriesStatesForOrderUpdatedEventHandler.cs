﻿using System.Threading;
using System.Threading.Tasks;
using Deliveries.Core.Application.Events;
using DeliveryDistribution.Integration.Contracts;
using MassTransit;
using MediatR;
using Microsoft.Extensions.Logging;

namespace Deliveries.Core.Application.EventHandlers
{
    public class PublishToBrokerWhenAllDeliveriesStatesForOrderUpdatedEventHandler:
        INotificationHandler<AllDeliveriesStatesForOrderUpdatedEvent>
    {
        private readonly IPublishEndpoint _publishEndpoint;
        private readonly ILogger<PublishToBrokerWhenAllDeliveriesStatesForOrderUpdatedEventHandler> _logger;

        public PublishToBrokerWhenAllDeliveriesStatesForOrderUpdatedEventHandler(
            IPublishEndpoint publishEndpoint, 
            ILogger<PublishToBrokerWhenAllDeliveriesStatesForOrderUpdatedEventHandler> logger)
        {
            _publishEndpoint = publishEndpoint;
            _logger = logger;
        }

        public async Task Handle(AllDeliveriesStatesForOrderUpdatedEvent notification, CancellationToken cancellationToken)
        {
            _logger.LogInformation("{@AllDeliveriesStatesForOrderUpdatedEvent} is publishing to broker", notification );
            await _publishEndpoint.Publish<AllDeliveriesStatesUpdatedIntegrationEvent>(new
            {
                notification.OrderId,
                notification.DeliveryStateId
            }, 
                cancellationToken);
            _logger.LogInformation("{@AllDeliveriesStatesForOrderUpdatedEvent} was published to broker", notification );
        }
    }
}