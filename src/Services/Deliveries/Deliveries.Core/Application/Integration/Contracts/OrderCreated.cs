﻿using System;

// ReSharper disable once CheckNamespace
namespace DeliveryDistribution.Integration.Contracts
{
    // ReSharper disable once InconsistentNaming
    public interface OrderCreated
    {
        public Guid OrderId { get;}
        
        public double Weight { get; }
    }
}