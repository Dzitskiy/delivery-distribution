﻿using System;
using MediatR;

namespace Deliveries.Core.Application.Events
{
    public class DeliveryStateUpdatedEvent:INotification
    {
        public Guid DeliveryId { get; set; }
        
        public int DeliveryStateId { get; set; }
    }
}