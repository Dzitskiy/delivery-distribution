﻿using AutoMapper;
using Deliveries.Core.Enumerations;
using Deliveries.WebHost.Dto;

namespace Deliveries.WebHost.Mappers
{
    public class AutoMapperStateProfiles: Profile
    {
        public AutoMapperStateProfiles()
        {
            CreateMap<DeliveryState, StateResponse>()
                .ForMember(dst => dst.State, 
                    opt => opt.MapFrom(src =>
                        src.Name));
        }
    }
}