﻿using Deliveries.WebHost.Dto.Delivery;
using FluentValidation;

namespace Deliveries.WebHost.Validation.Delivery
{
    public class UpdateCourierRequestValidator:AbstractValidator<UpdateCourierRequest>
    {
        public UpdateCourierRequestValidator()
        {
            RuleFor(r => r.CourierId).NotEmpty();
        }
    }
}