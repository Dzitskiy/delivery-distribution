﻿using Deliveries.WebHost.Dto.Delivery;
using FluentValidation;

namespace Deliveries.WebHost.Validation.Delivery
{
    public class UpdateStateRequestValidator: AbstractValidator<UpdateStateRequest>
    {
        public UpdateStateRequestValidator()
        {
            RuleFor(r => r.State).GreaterThanOrEqualTo(0);
        }
    }
}