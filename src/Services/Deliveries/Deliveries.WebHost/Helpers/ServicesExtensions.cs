﻿using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using Deliveries.Core.Application.Integration.Consumers;
using Deliveries.WebHost.Options;
using MassTransit;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.Processors.Security;

namespace Deliveries.WebHost.Helpers
{
    public static class ServicesExtensions
    {
        public static IServiceCollection AddMassTransitRabbitMq(
            this IServiceCollection services, IConfiguration configuration)
        {
            var rabbitSettings = configuration.GetSection(RabbitMqOptions.SectionName).Get<RabbitMqOptions>();
            services.AddMassTransit(x =>
            {
                x.AddConsumer<OrderCreatedEventConsumer>();
                x.AddConsumer<CheckCouriersDeliveriesConsumer>();
                x.AddConsumer<OrderStateUpdatedEventConsumer>();
                
                x.UsingRabbitMq(
                    (context, cfg) =>
                    {
                        cfg.Host(
                            rabbitSettings.Host,
                            rabbitSettings.Port,
                            rabbitSettings.VirtualHost,
                            conf =>
                            {
                                conf.Username(rabbitSettings.UserName);
                                conf.Password(rabbitSettings.Password);
                            });
                        cfg.ReceiveEndpoint(rabbitSettings.OrderCreatedEventQueue, e =>
                        {
                            e.ConfigureConsumer<OrderCreatedEventConsumer>(context);
                        });
                        cfg.ReceiveEndpoint(rabbitSettings.CheckCouriersDeliveriesQueue, e =>
                        {
                            e.ConfigureConsumer<CheckCouriersDeliveriesConsumer>(context);
                        });
                        cfg.ReceiveEndpoint(rabbitSettings.OrderStateUpdatedEventQueue, e =>
                        {
                            e.ConfigureConsumer<OrderStateUpdatedEventConsumer>(context);
                        });
                    }
                );
            });

            services.AddMassTransitHostedService();
            return services;
        }

        public static IServiceCollection AddCustomAuthentication(
            this IServiceCollection services, IConfiguration configuration)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Remove("sub");
            
            var identityUrl = configuration.GetValue<string>("IdentityHostUrl");
            
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.Authority = identityUrl;
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateAudience = false
                    };
                });
            return services;
        }

        public static IServiceCollection AddCustomAuthorization(
            this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddPolicy(Policies.DeliveriesAccess, policy =>
                    policy.RequireClaim("scope", "deliveries"));
            });
            return services;
        }

        public static IServiceCollection AddSwaggerWithAuth(
            this IServiceCollection services, IConfiguration configuration)
        {
            var identityExternalUrl = configuration.GetValue<string>("IdentityHostUrlExternal");
            services.AddOpenApiDocument(options =>
            {
                options.Title = "Deliveries API Doc";
                options.Version = "1.0";
                options.AddSecurity("oauth2", new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        Implicit  = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = $"{identityExternalUrl}/connect/authorize",
                            TokenUrl = $"{identityExternalUrl}/connect/token",
                            Scopes = new Dictionary<string, string> { { "deliveries", "Deliveries API" } }
                        }
                    }
                });
                
                options.OperationProcessors.Add(new OperationSecurityScopeProcessor("oauth2"));
                
            });
            return services;
        }
    }
}