﻿using System;

namespace Deliveries.WebHost.Dto.Delivery
{
    public class DeliveryShortResponse
    {
        public Guid Id { get; set; }
        public StateResponse DeliveryState { get; set; }
        public DateTime CreatedOn { get; set; }
    }
}
