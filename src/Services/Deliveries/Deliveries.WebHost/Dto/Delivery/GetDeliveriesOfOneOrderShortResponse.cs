﻿using System;

namespace Deliveries.WebHost.Dto.Delivery
{
    public class GetDeliveriesOfOneOrderShortResponse
    {
        /// <summary>
        /// Id заказа, для которого выполняется доставка.
        /// </summary>
        public Guid OrderId { get; set; }
        
        /// <summary>
        /// GUID доставки.
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Текущее состояние доставки.
        /// </summary>
        public StateResponse DeliveryState { get; set; }
        
        public DateTime CreatedOn { get; set; }
    }
}