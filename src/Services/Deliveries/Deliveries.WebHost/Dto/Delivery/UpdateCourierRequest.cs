﻿using System;

namespace Deliveries.WebHost.Dto.Delivery
{
    public record UpdateCourierRequest
    {
        public Guid CourierId { get; set; }
    }
}