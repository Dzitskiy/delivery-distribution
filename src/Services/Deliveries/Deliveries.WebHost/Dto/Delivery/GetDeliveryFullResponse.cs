﻿using System;

namespace Deliveries.WebHost.Dto.Delivery
{
    public class GetDeliveryFullResponse
    {
        /// <summary>
        /// GUID доставки.
        /// </summary>
        public Guid Id { get; set; }
        
        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreatedOn { get; set; }
        
        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime UpdatedOn { get; set; }
        
        /// <summary>
        /// Текущее состояние
        /// </summary>
        public StateResponse DeliveryState { get; set; }
        
        /// <summary>
        /// Id курьера, выполняющего доставку
        /// </summary>
        public Guid? CourierId { get; set; }
        
        /// <summary>
        /// Id заказа, для которого выполняется доставка
        /// </summary>
        public Guid OrderId { get; set; }
    }
}