using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Domain;
using Deliveries.DataAccess.Data;
using Microsoft.EntityFrameworkCore;

namespace Deliveries.DataAccess.Repositories
{
    public class DeliveriesRepository: IRepository<Delivery>
    {
        private readonly DataContext _dataContext;

        public DeliveriesRepository(DataContext context)
        {
            _dataContext = context;
        }
        
        public async Task<IEnumerable<Delivery>> GetAllAsync()
        {
            var entities = await _dataContext
                .Deliveries
                .Include(d => d.DeliveryState)
                .AsNoTrackingWithIdentityResolution()
                .ToListAsync();
            return entities;
        }

        public async Task<Delivery?> GetByIdAsync(Guid id)
        {
            var entity = await _dataContext
                .Deliveries
                .Include(d => d.DeliveryState)
                .FirstOrDefaultAsync(x => x.Id == id);
            return entity;
        }

        public async Task<Guid> AddAsync(Delivery entity)
        {
            _dataContext.Deliveries.Add(entity);
            await _dataContext.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(Delivery entity)
        {
            _dataContext.Deliveries.Update(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Delivery entity)
        {
            _dataContext.Deliveries.Remove(entity);
            await _dataContext.SaveChangesAsync();
        }
        
        public async Task<IEnumerable<Delivery>> GetOnConditionAsync(Expression<Func<Delivery, bool>> predicate)
        {
            return await _dataContext.Deliveries
                .Include(d => d.DeliveryState)
                .AsNoTrackingWithIdentityResolution()
                .Where(predicate)
                .ToListAsync();
        }
    }
}