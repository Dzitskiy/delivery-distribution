﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Domain;
using Deliveries.DataAccess.Data;
using Microsoft.EntityFrameworkCore;

namespace Deliveries.DataAccess.Repositories
{
    public class DeliveryStateHistoryRepository:IRepository<DeliveryStateHistory>
    {
        private readonly DataContext _dataContext;

        public DeliveryStateHistoryRepository(DataContext context)
        {
            _dataContext = context;
        }
        
        public async Task<IEnumerable<DeliveryStateHistory>> GetAllAsync()
        {
            return 
                await _dataContext.DeliveryStateHistories
                    .AsNoTracking()
                    .ToListAsync();
        }

        public async Task<DeliveryStateHistory?> GetByIdAsync(Guid id)
        {
            return await _dataContext.DeliveryStateHistories.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<Guid> AddAsync(DeliveryStateHistory entity)
        {
            _dataContext.DeliveryStateHistories.Add(entity);
            await _dataContext.SaveChangesAsync();
            return entity.Id;
        }

        public async Task UpdateAsync(DeliveryStateHistory entity)
        {
            _dataContext.DeliveryStateHistories.Update(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(DeliveryStateHistory entity)
        {
            _dataContext.DeliveryStateHistories.Remove(entity);
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IEnumerable<DeliveryStateHistory>> GetOnConditionAsync(Expression<Func<DeliveryStateHistory, bool>> predicate)
        {
            return await _dataContext.DeliveryStateHistories
                .Include(h => h.DeliveryState)
                .AsNoTrackingWithIdentityResolution()
                .Where(predicate)
                .ToListAsync();
        }
    }
}