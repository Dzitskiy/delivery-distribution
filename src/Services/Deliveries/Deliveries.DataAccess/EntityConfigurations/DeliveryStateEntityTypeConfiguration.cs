using Deliveries.Core.Enumerations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Deliveries.DataAccess.EntityConfigurations
{
    public class DeliveryStateEntityTypeConfiguration:
        IEntityTypeConfiguration<DeliveryState>
    {
        public void Configure(EntityTypeBuilder<DeliveryState> builder)
        {
            builder.HasKey(o => o.Id);

            builder.Property(o => o.Id)
                .HasDefaultValue(1)
                .ValueGeneratedNever()
                .IsRequired();

            builder.Property(o => o.Name)
                .HasMaxLength(200)
                .IsRequired();
        }
    }
}