using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Deliveries.Core.Abstraction.Repositories;
using Deliveries.Core.Application.Services;
using Deliveries.Core.Domain;
using Deliveries.UnitTests.Attributes;
using Deliveries.UnitTests.Builders.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Deliveries.UnitTests.Core.Application.Services.DeliveryServiceTests
{
    public class GetCourierDeliveriesAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task GetCourierDeliveriesAsync_DeliveriesExists_ReturnsResponse(
            [Frozen] Mock<IRepository<Delivery>> mockRepository,
            [Frozen] DeliveryService deliveryService)
        {
            //arrange
            var courier = Guid.Parse("fccbce2a-c1f9-45e6-b845-80360080db1c");
            var delivery = DeliveryBuilder
                .CreateBase()
                .WithCourier(courier);
            
            var deliveryList = new List<Delivery>()
            {
                delivery
            };
            mockRepository.Setup(repo => 
                    repo.GetOnConditionAsync(It.IsAny<Expression<Func<Delivery,bool>>>()))
                    .ReturnsAsync(deliveryList);
            
            //act
            var result = await deliveryService.GetCourierDeliveriesAsync(delivery.CourierId);
            
            //assert
            result.Should().BeEquivalentTo(deliveryList);
        }
    }
}