﻿using System;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Deliveries.Core.Abstraction;
using Deliveries.Core.Application.Services;
using Deliveries.UnitTests.Attributes;
using Deliveries.UnitTests.Builders.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Deliveries.UnitTests.Core.Application.Services.DeliveryStateHistoryServiceTests
{
    public class AddDeliveryStateAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task AddDeliveryStateAsync_CorrectInput_UpdatedOnIsNow(
            [Frozen] Mock<ICurrentDateTimeProvider> mockDateTime,
            [Frozen] DeliveryStateHistoryService sut)
        {
            //arrange
            var stateHistory = DeliveryStateHistoryBuilder.CreateBase()
                .WithDeliveryId(DeliveryBuilder.CreateBase().Id)
                .WithStateId(1);
            var now = new DateTime(2021, 03, 10);
            
            mockDateTime.Setup(mock => mock.CurrentDateTime)
                .Returns(now);
            
            //act
            await sut.AddDeliveryStateAsync(stateHistory);
            
            //assert
            stateHistory.UpdatedOn.Should().Be(now);
        }
    }
}