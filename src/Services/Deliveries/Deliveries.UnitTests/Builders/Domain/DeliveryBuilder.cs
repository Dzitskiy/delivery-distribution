using System;
using Deliveries.Core.Domain;
using Deliveries.Core.Enumerations;
using MassTransit.Logging;

namespace Deliveries.UnitTests.Builders.Domain
{
    public static class DeliveryBuilder
    {
        public static Delivery CreateBase()
        {
            return new Delivery()
            {
                Id = Guid.Parse("18ef4835-bac6-4b0c-85d9-09b14614eec8"),
                CreatedOn = new DateTime(2021, 01, 01),
                UpdatedOn = new DateTime(2021, 01, 01),
                DeliveryStateId = DeliveryState.Processing.Id,
                DeliveryState = DeliveryState.Processing,
                OrderId = Guid.Parse("1ad5e6d6-812f-4e6e-a54c-5aee4773ebb0")
            };
        }

        public static Delivery WithEmptyId(this Delivery delivery)
        {
            delivery.Id = Guid.Empty;
            return delivery;
        }

        public static Delivery WithState(this Delivery delivery, int state)
        {
            delivery.DeliveryStateId = state;
            return delivery;
        }
        
        public static Delivery WithCreatedOn(this Delivery delivery, DateTime createdOn)
        {
            delivery.CreatedOn = createdOn;
            return delivery;
        }

        public static Delivery WithUpdatedOn(this Delivery delivery, DateTime updatedOn)
        {
            delivery.UpdatedOn = updatedOn;
            return delivery;
        }

        public static Delivery WithCourier(this Delivery delivery, Guid courierId)
        {
            delivery.CourierId = courierId;
            return delivery;
        }
        
    }
}