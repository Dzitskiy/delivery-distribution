﻿using System;
using Deliveries.Core.Domain;

namespace Deliveries.UnitTests.Builders.Domain
{
    public static class DeliveryStateHistoryBuilder
    {
        public static DeliveryStateHistory CreateBase()
        {
            return new DeliveryStateHistory()
            {
                Id = Guid.Parse("5714dd11-75fa-4a6f-aa77-b339b0617d42"),
                UpdatedOn = new DateTime(2021, 03, 09)
            };
        }

        public static DeliveryStateHistory WithId(this DeliveryStateHistory stateHistory, Guid id)
        {
            stateHistory.Id = id;
            return stateHistory;
        }

        public static DeliveryStateHistory WithDeliveryId(this DeliveryStateHistory stateHistory, Guid deliveryId)
        {
            stateHistory.DeliveryId = deliveryId;
            return stateHistory;
        }

        public static DeliveryStateHistory WithStateId(this DeliveryStateHistory stateHistory, int stateId)
        {
            stateHistory.DeliveryStateId = stateId;
            return stateHistory;
        }

        public static DeliveryStateHistory WithUpdatedOn(this DeliveryStateHistory stateHistory, DateTime updatedOn)
        {
            stateHistory.UpdatedOn = updatedOn;
            return stateHistory;
        }
    }
}