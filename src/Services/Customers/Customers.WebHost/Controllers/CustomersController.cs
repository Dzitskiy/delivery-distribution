﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Customers.Core.Abstraction.Services;
using Customers.Core.Domain;
using Customers.WebHost.Dto.Customer;
using Microsoft.AspNetCore.Mvc;

namespace Customers.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public CustomersController(ICustomerService customerService, IMapper mapper)
        {
            _customerService = customerService;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCouriersAsync()
        {
            var customers = await _customerService.GetCustomersAsync();
            var response = _mapper.Map<IEnumerable<CustomerShortResponse>>(customers);
            return Ok(response);
        }

        /// <summary>
        /// Получить данные клиента по идентификатору
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}", Name = "GetCustomerAsync")]
        public async Task<ActionResult<CustomerDetailedResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerService.GetCustomerByIdAsync(id);
            var response = _mapper.Map<CustomerDetailedResponse>(customer);
            return Ok(response);
        }

        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddCourierAsync(CreateOrEditCustomerRequest request)
        {
            var newCustomer = _mapper.Map<Customer>(request);
            var responseId = await _customerService.AddCustomerAsync(newCustomer);
            return CreatedAtRoute(nameof(GetCustomerAsync), new { id = responseId }, responseId);
        }

        /// <summary>
        /// Изменить данные клиента по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var updatedCustomer = new Customer();
            _mapper.Map(request, updatedCustomer);
            await _customerService.EditCustomerAsync(id, updatedCustomer);
            return Ok();
        }

        /// <summary>
        /// Удалить клиента по идентификатору  
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerAsync(Guid id)
        {
            await _customerService.DeleteCustomerAsync(id);
            return Ok();
        }
    }
}