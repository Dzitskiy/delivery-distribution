namespace Customers.WebHost.Dto.Customer
{
    public record CreateOrEditCustomerRequest
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
    }
}