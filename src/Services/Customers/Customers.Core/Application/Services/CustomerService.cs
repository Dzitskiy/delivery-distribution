﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Customers.Core.Abstraction.Repositories;
using Customers.Core.Abstraction.Services;
using Customers.Core.Application.Exceptions;
using Customers.Core.Domain;
using Microsoft.Extensions.Logging;

namespace Customers.Core.Application.Services
{
    public class CustomerService : ICustomerService
    {

        private readonly IRepository<Customer> _customersRepository;
        private readonly ILogger<CustomerService> _logger;

        public CustomerService(
            IRepository<Customer> customersRepository,
            ILogger<CustomerService> logger)
        {
            _customersRepository = customersRepository;
            _logger = logger;
        }

        public async Task<IEnumerable<Customer>> GetCustomersAsync()
        {
            var customers = await _customersRepository.GetAllAsync();

            return customers;
        }

        public async Task<Customer> GetCustomerByIdAsync(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer == null)
            {
                _logger.LogWarning("Customer with id: {@CustomerId} not found", id);
                throw new EntityNotFoundException(id);
            }
            return customer;
        }

        public async Task<Guid> AddCustomerAsync(Customer newCustomer)
        {
            await _customersRepository.AddAsync(newCustomer);
            return newCustomer.Id;
        }

        public async Task EditCustomerAsync(Guid id, Customer updatedCustomer)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer == null)
            {
                _logger.LogWarning("Customer with id: {@CustomerId} not found", id);
                throw new EntityNotFoundException(id);
            }

            customer.Email = updatedCustomer.Email;
            customer.Name = updatedCustomer.Name;
            customer.PhoneNumber = updatedCustomer.PhoneNumber;

            await _customersRepository.UpdateAsync(customer);
        }

        public async Task DeleteCustomerAsync(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer == null)
            {
                _logger.LogWarning("Customer with id: {@CustomerId} not found", id);
                throw new EntityNotFoundException(id);
            }

            await _customersRepository.DeleteAsync(customer);
        }

    }
}
