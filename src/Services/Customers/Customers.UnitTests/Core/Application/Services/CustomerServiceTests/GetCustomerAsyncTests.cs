﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Customers.Core.Abstraction.Repositories;
using Customers.Core.Application.Services;
using Customers.Core.Domain;
using Customers.UnitTests.Attributes;
using Customers.UnitTests.Builders.Domain;
using FluentAssertions;
using Moq;
using Xunit;

namespace Customers.UnitTests.Core.Application.Services.CustomerServiceTests
{
    public class GetCustomersAsyncTests
    {
        [Theory, AutoMoqData]
        public async Task GetCustomersAsync_CustomerNotFound_ReturnsEmptyCollection(
            [Frozen] Mock<IRepository<Customer>> mockRepository,
            [Frozen] CustomerService customerService
            )
        {
            //Arrange
            mockRepository.Setup(repo => repo.GetAllAsync()).ReturnsAsync(new List<Customer>());
                
            //Act
            var result =  await customerService.GetCustomersAsync();

            //Assert
            result.Should().BeEmpty();
        }

        [Theory, AutoMoqData]
        public async Task GetCustomersAsync_CustomerFound_ShouldReturnFoundCustomerList(
            [Frozen] Mock<IRepository<Customer>> mockRepository,
            [Frozen] CustomerService customerService
            )
        {
            //Arrange
            var customer = CustomerBuilder.CreateBase();
            var customerList = new List<Customer>() {customer};
            mockRepository.Setup(repo => repo.GetAllAsync()).ReturnsAsync(customerList);

            //Act
            var result = await customerService.GetCustomersAsync();

            //Assert
            result.Should().BeEquivalentTo(customerList, opt => opt.ExcludingMissingMembers());
        }
    }
}