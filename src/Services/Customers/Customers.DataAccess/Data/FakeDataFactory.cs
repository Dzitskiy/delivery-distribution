﻿using System;
using System.Collections.Generic;
using Customers.Core.Domain;

namespace Customers.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Customer> Customers => new List<Customer>()
        {
            new Customer()
            {
                Id = Guid.Parse("5540c4e7-0d75-448c-bad4-68d00700fe98"),
                Name = "Джон Смит",
                PhoneNumber = "222222222",
                Email = "john@post.com"
            },
            new Customer()
            {
                Id = Guid.Parse("092d7ae5-8c71-448c-bfc8-13f2f3f9a80b"),
                Name = "Билли Бонс",
                PhoneNumber = "333333333",
                Email = "billy@post.com"
            },
        };
    }
}
