﻿// Copyright (c) Duende Software. All rights reserved.
// See LICENSE in the project root for license information.


using System;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using IdentityServer.Data;
using IdentityServerHost.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Serilog;

namespace IdentityServer
{
    public class SeedData
    {
        public static void EnsureSeedData(string connectionString)
        {
            var services = new ServiceCollection();
            services.AddLogging();
            services.AddDbContext<ApplicationDbContext>(options =>
               options.UseNpgsql(connectionString, o => o.MigrationsAssembly(typeof(Startup).Assembly.FullName)));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            using (var serviceProvider = services.BuildServiceProvider())
            {
                using (var scope = serviceProvider.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var context = scope.ServiceProvider.GetService<ApplicationDbContext>();
                    context.Database.Migrate();

                    var userMgr = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
                    var john = userMgr.FindByNameAsync("john").Result;
                    if (john == null)
                    {
                        john = new ApplicationUser
                        {
                            Id = Guid.Parse("5540c4e7-0d75-448c-bad4-68d00700fe98").ToString(),
                            UserName = "john",
                            Email = "john@post.com",
                            Name = "Джон",
                            LastName = "Смит",
                            EmailConfirmed = true
                        };
                        var result = userMgr.CreateAsync(john, "Pass123$").Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        result = userMgr.AddClaimsAsync(john, new Claim[]{
                            new Claim(JwtClaimTypes.Name, "Джон Смит"),
                            new Claim(JwtClaimTypes.GivenName, "Джон"),
                            new Claim(JwtClaimTypes.FamilyName, "Смит"),
                        }).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }
                        Log.Debug("john created");
                    }
                    else
                    {
                        Log.Debug("john already exists");
                    }

                    var bill = userMgr.FindByNameAsync("bill").Result;
                    if (bill == null)
                    {
                        bill = new ApplicationUser
                        {
                            Id = Guid.Parse("092d7ae5-8c71-448c-bfc8-13f2f3f9a80b").ToString(),
                            UserName = "bill",
                            Email = "billy@post.com",
                            Name = "Билли",
                            LastName = "Бонс",
                            EmailConfirmed = true
                        };
                        var result = userMgr.CreateAsync(bill, "Pass123$").Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }

                        result = userMgr.AddClaimsAsync(bill, new Claim[]{
                            new Claim(JwtClaimTypes.Name, "Билли Бонс"),
                            new Claim(JwtClaimTypes.GivenName, "Билли"),
                            new Claim(JwtClaimTypes.FamilyName, "Бонс"),
                            new Claim("location", "nowhere")
                        }).Result;
                        if (!result.Succeeded)
                        {
                            throw new Exception(result.Errors.First().Description);
                        }
                        Log.Debug("bill created");
                    }
                    else
                    {
                        Log.Debug("bill already exists");
                    }
                }
            }
        }
    }
}
