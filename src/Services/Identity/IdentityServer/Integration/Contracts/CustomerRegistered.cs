﻿// ReSharper disable once CheckNamespace
namespace DeliveryDistribution.Integration.Contracts
{
    // ReSharper disable once InconsistentNaming
    public interface CustomerRegistered
    {
        /// <summary>
        /// Id пользователя
        /// </summary>
        public string Id { get; set; }
        
        /// <summary>
        /// Полное имя пользователя
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Номер телефона.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
    }
}