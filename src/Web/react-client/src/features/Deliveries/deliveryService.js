import {createClient} from "../../api/http-client";

//Получить список доставки для выбранного заказа
const getByOrderId = orderId =>{
    return createClient().get(`/deliveries/order/${orderId}`);
};

export default{
    getByOrderId
};