import {
    createAsyncThunk,
    createSlice,
    createEntityAdapter
} from "@reduxjs/toolkit";

import OrderService from "./orderService";

import authService from "../Auth/AuthorizeService";

const ordersAdapter = createEntityAdapter({
    sortComparer: (a, b) => b.createdOn.localeCompare(a.createdOn),
  })

  const orderInitialState = {
    id: "",
    code: "",
    departureAddress: "",
    destinationAddress: "",
    weight: 0,
    price: 0,
    orderState: {
      id: 0,
      state: ""
    },
    isPaid: false
};

const initialState = ordersAdapter.getInitialState({
    selectedOrder:{
        ...orderInitialState,
        status: "idle",
        error: null
    },
    status: "idle",
    error: null
});


export const fetchOrdersByCustomer = createAsyncThunk(
    'orders/fetchOrdersByCustomer',
    async() => {
        const user = await authService.getUser();
        const response = await OrderService.getByCustomerId({
            customerId: user.sub
        });
        return response.data;
    }
)


export const addOrder = createAsyncThunk(
    'orders/addOrder',
    async (data) => {
        const user = await authService.getUser();
        await OrderService.create({
            ...data,
            customerId: user.sub
        });
    }
);

export const getOrderById = createAsyncThunk(
    'orders/getOrderById',
    async (orderId) =>{
        const response = await OrderService.getById(orderId);
        return response.data;
    }
)

export const setPaidById = createAsyncThunk(
    'orders/setPaidById',
    async(orderId) =>{
        await OrderService.setPaidById(orderId);
    }
);

export const cancelOrderById = createAsyncThunk(
    'orders/cancelOrderById',
    async(orderId, updateDeliveries) =>{
        await OrderService.cancelOrderById(orderId);
        updateDeliveries();
    }
)

const orderSlice = createSlice({
        name: "orders",
        initialState,
        reducers:{
            //Обновляем список всех заказов, когда изменился выбранный заказ
            selectedOrderUpdated(state, action) {
                state.status = "idle";
            }
        },
        extraReducers:{

            [addOrder.fulfilled]: (state, action) =>
            {
                state.status = "idle";
            },
            [addOrder.rejected]: (state, action) =>{
                state.status = "failed";
                state.error = action.error.message;
            },

            [fetchOrdersByCustomer.pending]: (state, action) =>{
                state.status = "loading";
            },
            [fetchOrdersByCustomer.fulfilled]: (state, action) =>{
                state.status = "succeeded";
                ordersAdapter.upsertMany(state, action.payload);
            },
            [fetchOrdersByCustomer.rejected]: (state, action) =>{
                state.status = "failed";
                state.error = action.error.message;
            },

            [getOrderById.fulfilled]: (state, action) =>{
                state.selectedOrder = {...action.payload, status: "succeeded"};
            },
            [getOrderById.pending]: (state, action) =>{
                state.selectedOrder.status = "loading";
            },
            [getOrderById.rejected]: (state, action) =>{
                state.selectedOrder.status = "failed";
                state.selectedOrder.error = action.error.message;
            },

            [setPaidById.fulfilled]: (state, action) =>{
                state.selectedOrder.isPaid = true;
                state.entities[state.selectedOrder.id].isPaid = true;
            },
            [setPaidById.pending]: (state, action) =>{
                state.selectedOrder.status = "loading";
            },
            [cancelOrderById.fulfilled]: (state, action) =>{
                state.entities[state.selectedOrder.id].orderState = state.selectedOrder.orderState;
            }

        }
});

export default orderSlice.reducer;

export const {selectedOrderUpdated} = orderSlice.actions;

export const {
    selectAll: selectAllOrders,
    selectById: selectOrderById,
    selectIds: selectOrderIds
  } = ordersAdapter.getSelectors((state) => state.orders);
