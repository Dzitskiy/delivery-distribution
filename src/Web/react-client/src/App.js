import React, { useEffect } from 'react';

import { BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import { Navbar, Nav} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap'
import CreateOrder from './features/Orders/createOrder';
import OrdersList from './features/Orders/ordersList';
import OrderDetails from './features/Orders/orderDetails';

import AuthorizeRoute from "./features/Auth/AuthorizeRoute";
import { ApplicationPaths } from "./features/Auth/ApiAuthorizationConstants";
import ApiAuthorizationRoutes from "./features/Auth/ApiAuthorizationRoutes";
import {LoginMenu} from "./features/Auth/LoginMenu";

function App() {

  useEffect(() =>
  {
    document.title = "Delivery distribution";
  });

  return (
    <Router>

        <Navbar collapseOnSelect expand="lg" bg="primary" variant="dark">
          <Navbar.Brand as={Link} to="/order">Delivery distribution</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto" variant="dark">
              <LinkContainer to="/order">
                <Nav.Link>Рассчитать доставку</Nav.Link>
              </LinkContainer>

              <LinkContainer to="/orderlist">
                <Nav.Link>Мои заказы</Nav.Link>
              </LinkContainer>

            </Nav>

          </Navbar.Collapse>

        <LoginMenu/>

        </Navbar>
      
        <Switch>

          {/* <Route exact path="/">
            <CreateOrder/>
          </Route> */}

          <AuthorizeRoute exact path="/order" component={CreateOrder}/>

          <AuthorizeRoute exact path="/order/:orderId" component={OrderDetails}/>

          <AuthorizeRoute path="/orderList" component={OrdersList}/>

          <Route path={ApplicationPaths.ApiAuthorizationPrefix} component={ApiAuthorizationRoutes}/>

        </Switch>
     
    </Router>
  );
}

export default App;
