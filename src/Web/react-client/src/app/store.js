import { configureStore } from '@reduxjs/toolkit';
import orderReducer from "../features/Orders/orderSlice";
import deliveryReducer from "../features/Deliveries/deliveriesSlice";

export default configureStore({
    reducer:{
        orders: orderReducer,
        deliveries: deliveryReducer
    }
});